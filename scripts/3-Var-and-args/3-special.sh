#!/bin/bash

echo '#####################'

echo "The filename of the current script -- $0"

echo '#####################'

echo "\$n - These variables correspond to the arguments with which a script was invoked - $1 $2 $3 "

echo '#####################'

echo "The number of arguments supplied to a script is $#"

echo '#####################'

echo "All the arguments are double quoted -- $*"

echo '#####################'

echo "All the arguments are individually double quoted -- $@"

echo '#####################'

echo "The exit status of the last command executed -- $?"

echo '#####################'

echo "The process number of the current shell -- $$"

echo '#####################'

echo "The process number of the last background command -- $!"
